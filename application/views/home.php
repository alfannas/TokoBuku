<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container" style="padding-top:10px; text-align: center; padding-right: 50px; padding-bottom:10px;">
    <h2>List Buku Tersedia</h2>
</div>
<div class="container-fluid" style="background-color:white; width:auto;" >
		<?php foreach($tampil_buku as $tb):?>
                      	<div class="col-md-3 col-sm-4 mb" >
                      		<div class="white-panel pn" background-color="black" style="margin-top:20px; margin-bottom:20px; width:auto;height:auto;">
                      			<div class="white-header" style="padding:20px!important; color:black!important;background-color:#e8e8e8 !important;font-weight:bold!important; text-align:center!important;">

                      				<font color="#00b758"><b>
						  			<h4><?=$tb->judul_buku?></h4>
						  		</b>
						  		</font>
									</p>	
	                      		<div class="centered">
										<img src="<?=base_url('assets/img/')?><?=$tb->foto_cover?>" width="120" height="160">
	                      		</div>
								  <div class="white-header" style="height:auto;margin-top:10px;color:black!important;">
								  	<center><i>Stok Buku = <?=$tb->stok?></i></center> 
								  <b><h3><?="Rp. ".number_format($tb->harga,0,",",".")?></h3></b>
                      			</div>
                      	</div>
                      </div>
                  </div>
						
		<?php endforeach ?>
</div>   	