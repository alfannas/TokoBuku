<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('login')!=TRUE) {
			redirect('User','refresh');
		}
		$this->load->model('m_buku','buku');
	}
	
	public function index()
	{
	
		$data['tampil_buku']=$this->buku->tampil();
		$data['kategori']=$this->buku->data_kategori();
		$data['konten']="home";
		$data['judul']="Daftar Buku";
		$this->load->view('template', $data);
	}

	public function toko()
	{
		$data['tampil_buku']=$this->buku->tampil();
		$data['kategori']=$this->buku->data_kategori();
		$data['konten']="toko";
		$data['judul']="Toko Togamedia";
		$this->load->view('template', $data);
	}
}